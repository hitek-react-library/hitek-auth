import AsyncStorage from '@react-native-async-storage/async-storage';
export async function getItem() {
  const value = await AsyncStorage.getItem('token');
  return value ? JSON.parse(value) : null;
}
export async function setItem(value: any) {
  return AsyncStorage.setItem('token', JSON.stringify(value));
}
export async function getMethod() {
  const value = await AsyncStorage.getItem('method');
  return value ? JSON.parse(value) : null;
}
export async function setMethod(value: any) {
  return AsyncStorage.setItem('method', JSON.stringify(value));
}
export async function removeItem() {
  await AsyncStorage.removeItem('token');
  await AsyncStorage.removeItem('method');
  return;
}
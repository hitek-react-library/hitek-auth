


export enum SignInMethod {
    GOOGLE = 1,
    FACEBOOK,
    APPLE,
    NAVER,
    KAKAO,
    EMAIL_AND_PASSWORD,
    PHONE_CODE,
    GUEST
}
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import {
  LoginMethod,
  LOGIN_METHOD,
  SOCIAL_CONFIG
} from '../utils/constants';
import { fetch } from '../utils/fetch';
import { print } from '../utils/log';
import { read, write } from './local_service';

export interface GoogleConfig {
  webClientId: String
}

const getTokenGoogle = (clientId: string) => {
  return new Promise(async (resolve, reject) => {
    print(clientId);
    GoogleSignin.configure({
      webClientId: clientId,
      offlineAccess: false,
    });
    try {
      await GoogleSignin.signOut();
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      resolve(userInfo.idToken);
    } catch (error: any) {
      if (error.toString().includes('cancelled') || error.toString().includes('user canceled')) {
        reject('CANCELLED');
      } else {
        reject(error);
      }

      // if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      //     console.log('🚀 ~ file: index.js ~ line 152 ~ Login ~ SIGN_IN_CANCELLED');
      // } else if (error.code === statusCodes.IN_PROGRESS) {
      //     console.log('🚀 ~ file: index.js ~ line 155 ~ Login ~ IN_PROGRESS');
      // } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      //     console.log('🚀 ~ file: index.js ~ line 158 ~ Login ~ PLAY_SERVICES_NOT_AVAILABLE');
      // } else {
      //     console.log('🚀 ~ file: index.js ~ line 161 ~ Login ~ ERROR', error);
      // }
    }
  });
};

export async function googleLogin<Type>(): Promise<Type> {
  const config = await read(SOCIAL_CONFIG);
  const token = await getTokenGoogle(config.google.webClientId);
  write(LOGIN_METHOD, LoginMethod.GOOGLE);
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST',
      path: `/auth/login_with_google`,
      body: {
        googleToken: token,
      }
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          // const token = res.data.results.token;
          // print('@got token', token);
          // write(TOKEN, token);
          // write(USER, res.data.results.object);
          // accept(res.data.results.object);
          accept(res.data.results);
        } else {
          reject(res.data);
        }
      })
      .catch((err: any) => {
        print('@err', err);
        reject(err);
      });
  });
}

export function signOutGoogle() {
  GoogleSignin.signOut();
}

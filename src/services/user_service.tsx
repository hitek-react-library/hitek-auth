import { fetch } from "../utils/fetch";
import { print } from "../utils/log";


export async function getProfile<Type>() : Promise<Type> {
    return new Promise((accept, reject) => {
      fetch({
        method: 'GET',
        path: `/user/profile`,
      }).then((res: any) => {
          if (res.data.code === 200) {
            accept(res.data.results.object);
          } else {
            reject(res.data);
          }
        })
        .catch((err) => {
          print('@err', err);
          reject(err);
        });
    });
}

export async function updateProfile<Type>(body: Object): Promise<Type> {
    return new Promise((accept, reject) => {
      fetch({
        method: 'PUT', 
        path: `/user`, 
        body: body
    })
        .then((res: any) => {
          if (res.data.code === 200) {
            accept(res.data.results.object);
          } else {
            reject(res.data);
          }
        })
        .catch((err) => {
          print('@err', err);
          reject(err);
        });
    });
}

// export async function forgotPassword(phoneOrEmail: string) {
//     return new Promise((accept, reject) => {
//       fetch({
//         method: 'POST', 
//         path: `/auth/forgot_password`, 
//         query: {
//           email: phoneOrEmail,
//         }
//       })
//         .then((res: any) => {
//           if (res.data.code === 200) {
//             accept(res.data.results.object);
//           } else {
//             reject(res.data);
//           }
//         })
//         .catch((err) => {
//           print('@err', err);
//           reject(err);
//         });
//     });
// }

// export async function validateCode(code: string, hashCode: string) {
//   return new Promise((accept, reject) => {
//     fetch({
//       method: 'POST', 
//       path: `/auth/validate_code`, 
//       query: {
//         code,
//         hash_code: hashCode
//       }
//     })
//       .then((res: any) => {
//         if (res.data.code === 200) {
//           accept(res.data.results.object);
//         } else {
//           reject(res.data);
//         }
//       })
//       .catch((err) => {
//         print('@err', err);
//         reject(err);
//       });
//   });
// }

export async function changePasswordByToken(token: string, password: string) {
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST', 
      path: `/auth/change-password`, 
      body: {
        token,
        password
      }
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          accept(true);
        } else {
          reject(false);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(false);
      });
  });
}

export async function verifyEmailStep1(email: string) {
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST', 
      path: `/auth/verify-email`, 
      body: {
        email
      }
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          accept(true);
        } else {
          reject(false);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(false);
      });
  });
}

export async function verifyEmailStep2(email: string, code: number) {
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST', 
      path: `/auth/verify-code`, 
      body: {
        email,
        code
      }
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          accept(true);
        } else {
          reject(false);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(false);
      });
  });
}

export async function resetPasswordViaDeepLink(email: string) {
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST', 
      path: `/auth/forgot-password`, 
      body: {
        email
      }
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          accept(true);
        } else {
          reject(false);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(false);
      });
  });
}

export async function generatePasswordViaEmail(email: string) {
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST', 
      path: `/auth/generate-new-password`, 
      body: {
        email
      }
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          accept(true);
        } else {
          reject(false);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(false);
      });
  });
}

/// birthday in format yymmdd
export async function findMyAccount(phone: string, birthday: string) {
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST', 
      path: `/auth/find-my-account`, 
      body: {
        phone,
        birthday
      }
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          accept(res.data.results.object);
        } else {
          reject(res.data);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(err);
      });
  });
}

import auth from '@react-native-firebase/auth';
import { print } from '../utils/log';
import { LOGIN_METHOD } from '../utils/constants';
import { LoginMethod } from '../utils/constants';
import { write } from './local_service';
import { fetch } from '../utils/fetch';

export enum PhoneLoginStatus {
  CODE_SENT = 1, AUTO_VERIFIED, AUTO_VERIFY_TIMEOUT, ERROR
}

export class PhoneLoginSupport {
  static myInstance: PhoneLoginSupport;
  confirmation: any;
  verificationId: string | undefined;
  onStatusChange: Function | undefined;

  /**
   * @returns {PhoneLoginSupport}
   */
  static getInstance() {
    if (PhoneLoginSupport.myInstance == null) {
      PhoneLoginSupport.myInstance = new PhoneLoginSupport();
    }

    return this.myInstance;
  }

  // registerAutoVerification = (autoCb: Function) => {
  //   auth().onAuthStateChanged(async (user) => {
  //     user?.providerId;
  //     if (user) {
  //       const token = await user.getIdToken();
  //       autoCb(token);
  //     }
  //   });
  // };

  signPhoneNumber = async (phone: string) => {
    // this.confirmation = await auth().signInWithPhoneNumber(phone);
    return new Promise((accept, reject) => {
      auth()
      .verifyPhoneNumber(phone, 120, true)
      .on('state_changed', (phoneAuthSnapshot) => {
        switch (phoneAuthSnapshot.state) {
          case auth.PhoneAuthState.CODE_SENT:
            this.verificationId = phoneAuthSnapshot.verificationId;
            if (this.onStatusChange) this.onStatusChange(PhoneLoginStatus.CODE_SENT);
            accept(true);
            // console.log('@code sent');
            break;
          case auth.PhoneAuthState.AUTO_VERIFIED:
            // console.log('@auto verify');
            // console.log(`@code ${phoneAuthSnapshot.code}`);
            // console.log(`@verification id ${phoneAuthSnapshot.verificationId}`);
            if (this.onStatusChange) this.onStatusChange(PhoneLoginStatus.AUTO_VERIFIED);
            break;
          case auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT:
            // console.log('@timeout');
            reject("timeout");
            if (this.onStatusChange) this.onStatusChange(PhoneLoginStatus.AUTO_VERIFY_TIMEOUT);
            break;
          case auth.PhoneAuthState.ERROR:
            // console.log('@verification error', phoneAuthSnapshot.error);
            reject(phoneAuthSnapshot.error);
            if (this.onStatusChange) this.onStatusChange(PhoneLoginStatus.ERROR, phoneAuthSnapshot.error);
            break;
        }
      })
    });
  };

  // resend = async (phone: string) => {
  //   // this.confirmation = await auth().signInWithPhoneNumber(phone, true);
  //   this.confirmation = await auth()
  //     .verifyPhoneNumber(phone, true)
  //     .on('state_changed', this.verificationStateChange);
  //   return;
  // };

  confirmCode = async (code: string) => {
    try {
      // const res = await this.confirmation.confirm(code);
      const credential = auth.PhoneAuthProvider.credential(
        this.verificationId ?? "",
        code
      );
      const userData = await auth().signInWithCredential(credential);
      const token = await userData?.user.getIdToken();
      print(JSON.stringify(userData?.user));
      print('@token', token);
      return token;
    } catch (error) {
      print('@error', error.code);
      throw new TypeError(error.code);
    }
  };

  loginWithToken<Type>(token: string): Promise<Type> {
    write(LOGIN_METHOD, LoginMethod.PHONE);
    return new Promise((accept, reject) => {
      fetch({
        method: 'POST',
        path: `/auth/login_with_phone`,
        body: {
          firebaseToken: token,
        },
      })
        .then((res: any) => {
          if (res.data.code === 200) {
            // const token = res.data.results.token;
            // print('@got token', token);
            // write(TOKEN, token);
            // write(USER, res.data.results.object);
            accept(res.data.results);
          } else {
            reject(res.data);
          }
        })
        .catch((err) => {
          print('@err', err);
          reject(err);
        });
    });
  }
}

export async function loginWithFbToken<Type>(token: string): Promise<Type> {
  write(LOGIN_METHOD, LoginMethod.PHONE);
  return new Promise((accept, reject) => {
    fetch({
      method: 'POST',
      path: `/auth/login_with_phone`,
      body: {
        firebaseToken: token,
      },
    })
      .then((res: any) => {
        if (res.data.code === 200) {
          // const token = res.data.results.token;
          // print('@got token', token);
          // write(TOKEN, token);
          // write(USER, res.data.results.object);
          accept(res.data.results);
        } else {
          reject(res.data);
        }
      })
      .catch((err) => {
        print('@err', err);
        reject(err);
      });
  });
}
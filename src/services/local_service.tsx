import AsyncStorage from '@react-native-async-storage/async-storage';

export async function write(key: string, value: any) {
    try {
        if (typeof value === 'object') {
            value = JSON.stringify(value)
        }
        await AsyncStorage.setItem(key, value.toString());
    } catch (error) {
    // Error saving data
    }
    return
}

export async function read(key: string) {
    try {
        const value = (await AsyncStorage.getItem(key)) ?? "";
        return parseJson(value);

        // return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch(e) {
    // error reading value
    }
    return undefined;
}

function parseJson(str: string) {
    try {
        return JSON.parse(str);
    } catch (e) {
        return str;
    }
}
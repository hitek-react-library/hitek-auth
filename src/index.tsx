import { AuthProvider, useAuthorization } from "./base/auth_provider";
import { SignInMethod } from "./base/method_enum";
import { getItem } from "./base/storage";
import type { GoogleConfig } from "./services/google_service";
import { getKakaoProfile } from './services/kakao_service';
import { write } from "./services/local_service";
import type { NaverConfig } from "./services/naver_service";
import { register } from "./services/other_login_service";
import { PhoneLoginSupport } from "./services/phone_login_service";
import { changePasswordByToken, findMyAccount, generatePasswordViaEmail, getProfile, resetPasswordViaDeepLink, updateProfile, verifyEmailStep1, verifyEmailStep2 } from "./services/user_service";
import { HOST, LOG_MODE, SOCIAL_CONFIG } from "./utils/constants";
import { uploadImage } from "./utils/fetch";

interface ConfigProps {
  host: string;
  log: boolean;
  google?: GoogleConfig;
  naver?: NaverConfig;
}
function setConfig(config: ConfigProps) {
  write(HOST, config.host);
  write(LOG_MODE, config.log);
  write(SOCIAL_CONFIG, config);
}

export {
  AuthProvider,
  useAuthorization,
  setConfig,
  SignInMethod,
  PhoneLoginSupport,
  findMyAccount,
  generatePasswordViaEmail,
  resetPasswordViaDeepLink,
  changePasswordByToken,
  verifyEmailStep1,
  verifyEmailStep2,
  register,
  getProfile,
  updateProfile,
  uploadImage,
  getItem as getToken,
  getKakaoProfile
};

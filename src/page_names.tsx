function createEnum<T extends { [P in keyof T]: P }>(o: T) {
    return o;
}

export const PAGE_NAME = createEnum({
    HLoginPage: 'HLoginPage',
    HPhoneLoginPage: 'HPhoneLoginPage',
    HVerifyCodePage: 'HVerifyCodePage',
    HChangePasswordPage: 'HChangePasswordPage',
    HProfilePage: 'HProfilePage',
  });
  

interface Account {
    id: string;
    username?: string;
    email?: string;
    nickname?: string;
  }
  
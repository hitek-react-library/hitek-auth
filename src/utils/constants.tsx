
export const HOST = "@host";
export const LOG_MODE = "@log-mode";
export const TOKEN = "@token";
export const USER = "@user";
export const SOCIAL_CONFIG = "@social-config";
export const LOGIN_METHOD = "@login-method";
export const GOOGLE_CONFIG = "@google-config";
export const NAVER_CONFIG = "@naver-config";
export const FACEBOOK_CONFIG = "@facebook-config";
export const KAKAO_CONFIG = "@kakao-config";
export const APPLE_CONFIG = "@apple-config";
export enum LoginMethod {
    GOOGLE, 
    FACEBOOK, 
    TWITTER, 
    NAVER, 
    KAKAO, 
    PHONE, 
    EMAIL_AND_PASSWORD, 
    APPLE
}
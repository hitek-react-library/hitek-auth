import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { useAuthorization, getProfile } from 'hitek-auth';
import { Button } from './components/buttons';
import { print } from '../utils/log';

const SignOut = (props: any) => {
  const { navigation } =  props;
  const { signOut } = useAuthorization();
  function navigateAuth() {
    signOut();
    navigation.navigate('Login');
  }
  return <Button text="Sign Out" onPressed={navigateAuth} />;
};

export const HomeScreen = (props: any) => {
  const { isLogged, authToken, user } = useAuthorization();


  const fetchProfile = async() => {
    try {
      const user = await getProfile<Account>();
      print('@user', user);
      // setResult('@profile: ' + user.username);
    } catch (e) {
      // setResult('@error: ' + e.message);
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text}>welcome : {user?.username}</Text>
      <Text style={styles.text}>status : {isLogged ? 'true' : 'false'}</Text>
      <Text style={styles.text}>
        authToken : {authToken ? authToken : 'null'}
      </Text>
      <View style={styles.actions}>
        <SignOut {...props} />
      </View>
      <Button text={"Get profile"} onPressed={fetchProfile} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  detailsContainer: {
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  button: {
    margin: 24,
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 20,
  },
  text: {
    textAlign: 'center',
  },
});

import { View, Text, SafeAreaView, TextInput } from 'react-native';
import React from 'react'
import { print } from '../utils/log';
import { findMyAccount } from 'hitek-auth';
import { Button } from './components/buttons';

export const FindAccountScreen = () => {
  const [phone, setPhone] = React.useState('');
  const [bod, setBod] = React.useState('');
  const [message, setMessage] = React.useState('');

  const resetPassword = async () => {
    try {
      setMessage('');
      const response = await findMyAccount(phone, bod);
      print("@response", response);
      setMessage(`Found email ${response}`);
      // navigation.replace("Home");
    } catch(e) {
      setMessage("Got error!");
      console.log("@ERR", e)
    }
    return;
  }

  return (
    <SafeAreaView style={{ flex: 1, justifyContent: 'center' }}>
      <View style={{ alignSelf: 'center' }}>
        <Text>Find your account based on phone and birthday</Text>
        <TextInput
          style={{
            backgroundColor: '#e6e6e6',
            fontSize: 24,
            minWidth: 300,
            marginVertical: 24,
            paddingHorizontal: 12,
            alignSelf: 'center',
          }}
          keyboardType={'numeric'}
          placeholder={'your phone number'}
          onChangeText={(v) => setPhone(v)}
        />
        <TextInput
          style={{
            backgroundColor: '#e6e6e6',
            fontSize: 24,
            minWidth: 300,
            marginVertical: 24,
            paddingHorizontal: 12,
            alignSelf: 'center',
          }}
          keyboardType={'number-pad'}
          placeholder={'yymmdd'}
          onChangeText={(v) => setBod(v)}
        />
        <Text style={{color: message.includes('error') ? 'red' : 'green'}}>{message}</Text>
        <Button
          text={"Find my account"}
          onPressed={resetPassword}
        />
      </View>
    </SafeAreaView>
  );
};

import { Text, TouchableOpacity } from 'react-native';
import React from 'react';

interface ButtonProp {
  text: string;
  onPressed?: Function;
}

export const Button: React.FC<ButtonProp> = ({
  text,
  onPressed,
}) => {
  return (
    <TouchableOpacity onPress={() => (onPressed ? onPressed() : null)}>
      <Text
        style={{
          textAlign: 'center',
          padding: 8,
          margin: 4,
          borderColor: 'blue',
          borderRadius: 12,
          borderWidth: 1,
          color: 'blue',
        }}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};


export const NoBorderButton: React.FC<ButtonProp> = ({
  text,
  onPressed,
}) => {
  return (
    <TouchableOpacity onPress={() => (onPressed ? onPressed() : null)}>
      <Text
        style={{
          textAlign: 'center',
          padding: 14,
          marginVertical: 8,
          color: 'blue',
        }}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};

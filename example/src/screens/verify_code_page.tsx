import { View, Text, SafeAreaView, TextInput } from 'react-native';
import React from 'react'
import { print } from '../utils/log';
import { PhoneLoginSupport, useAuthorization } from 'hitek-auth';
import { Button, NoBorderButton } from './components/buttons';

export const VerifyCodePage = (props: any) => {
  const {navigation, route} = props;
  const { signInWithFbToken  } = useAuthorization();
  const [code, setCode] = React.useState('');

  const verifyCode = async () => {
    try {
      const token = await PhoneLoginSupport.getInstance().confirmCode(code);
      const user = await signInWithFbToken(token ?? "");
      print("@user", user);
      navigation.replace("Home");
    } catch(e) {
      console.log("@ERR", e.message)
    }
    return;
  }

  return (
    <SafeAreaView style={{ flex: 1, justifyContent: 'center' }}>
      <View style={{ alignSelf: 'center' }}>
        <Text>Plz enter 6 number sent to phone {route.params.phone}</Text>
        <TextInput
          style={{
            backgroundColor: '#e6e6e6',
            fontSize: 24,
            minWidth: 120,
            marginVertical: 24,
            alignSelf: 'center',
            textAlign: 'center',
          }}
          maxLength={6}
          keyboardType="numeric"
          onChangeText={(code) => setCode(code)}
        />
        <Button
          text={"Verify"}
          onPressed={verifyCode}
        />
        <NoBorderButton text={"Resend"} onPressed={() => {}} />
      </View>
    </SafeAreaView>
  );
};

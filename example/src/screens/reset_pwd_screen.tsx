import { View, Text, SafeAreaView, TextInput } from 'react-native';
import React from 'react'
import { print } from '../utils/log';
import { generatePasswordViaEmail } from 'hitek-auth';
import { Button } from './components/buttons';

export const ResetPasswordScreen = () => {
  const [email, setEmail] = React.useState('');
  const [message, setMessage] = React.useState('');

  const resetPassword = async () => {
    try {
      setMessage('');
      const response = await generatePasswordViaEmail(email);
      print("@response", response);
      setMessage("Sent! Please check your email");
      // navigation.replace("Home");
    } catch(e) {
      setMessage("Got error!");
      console.log("@ERR", e)
    }
    return;
  }

  return (
    <SafeAreaView style={{ flex: 1, justifyContent: 'center' }}>
      <View style={{ alignSelf: 'center' }}>
        <Text>We will generate new password and send to your email</Text>
        <TextInput
          style={{
            backgroundColor: '#e6e6e6',
            fontSize: 24,
            minWidth: 300,
            marginVertical: 24,
            paddingHorizontal: 12,
            alignSelf: 'center',
          }}
          placeholder={'your-email@email.com'}
          onChangeText={(v) => setEmail(v)}
        />
        <Text style={{color: message.includes('error') ? 'red' : 'green'}}>{message}</Text>
        <Button
          text={"Generate password"}
          onPressed={resetPassword}
        />
      </View>
    </SafeAreaView>
  );
};

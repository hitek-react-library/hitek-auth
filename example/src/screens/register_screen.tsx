import { View, Text, SafeAreaView, TextInput, StyleSheet } from 'react-native';
import React from 'react';
import { print } from '../utils/log';
import { register as registerNewAccount } from 'hitek-auth';
import { Button } from './components/buttons';

export const RegisterScreen = (props: any) => {
  const { navigation } = props;
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [nickname, setNickname] = React.useState('');
  const [phone, setPhone] = React.useState('');

  const register = async () => {
    try {
      const user = await registerNewAccount({
        email,
        password,
        nickname,
        phone,
      });
      print('@user', user);
      navigation.replace('Home');
    } catch (e) {
      console.log('@ERR', e.message);
    }
    return;
  };

  return (
    <SafeAreaView style={{ flex: 1, justifyContent: 'center' }}>
      <View style={{ alignSelf: 'center' }}>
        <Text>Register new account</Text>
        <TextInput
          style={styles.inputStyle}
          placeholder={'Email'}
          onChangeText={(v) => setEmail(v)}
        />
        <TextInput
          style={styles.inputStyle}
          placeholder={'Password'}
          onChangeText={(v) => setPassword(v)}
        />
        <TextInput
          style={styles.inputStyle}
          placeholder={'Nickname'}
          onChangeText={(v) => setNickname(v)}
        />
        <TextInput
          style={styles.inputStyle}
          placeholder={'Phone'}
          onChangeText={(v) => setPhone(v)}
        />
        <Button text={'Register'} onPressed={register} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    backgroundColor: '#e6e6e6',
    fontSize: 24,
    minWidth: 300,
    marginVertical: 8,
    alignSelf: 'center',
    textAlign: 'center',
  },
});

import { StyleSheet, Text, TextInput, View } from 'react-native';
import React, { useState } from 'react';
import { Button, NoBorderButton } from './components/buttons';
import { PhoneLoginSupport, SignInMethod, useAuthorization } from 'hitek-auth';

export const LoginScreen = (props: any) => {
  const { navigation } = props;
  const { signIn, guestSignIn } = useAuthorization();

  const loginWithSocial = async (method: number) => {
    try {
      const account: Account = await signIn(method);
      console.log(account);
      navigation.navigate('Home');
    } catch (e) {
      console.log('@error', e);
    }
  };

  const loginWithGuest = async () => {
    try {
      const account: Account = await guestSignIn("1234567890");
      console.log(account);
      navigation.navigate('Home');
    } catch (e) {
      console.log('@error', e);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 24, marginVertical: 80 }}>
        Welcome to Hitek Lib
      </Text>
      <UsernamePasswordLoginView {...props} style={{ padding: 18 }} />
      <PhoneLoginView {...props} style={{ paddingBottom: 18 }} />
      <View style={styles.socialContainer}>
        <Button
          text={'GOOGLE'}
          onPressed={() => loginWithSocial(SignInMethod.GOOGLE)}
        />
        <Button
          text={'FACEBOOK'}
          onPressed={() => loginWithSocial(SignInMethod.FACEBOOK)}
        />
        <Button
          text={'APPLE'}
          onPressed={() => loginWithSocial(SignInMethod.APPLE)}
        />
        <Button
          text={'KAKAO'}
          onPressed={() => loginWithSocial(SignInMethod.KAKAO)}
        />
        <Button
          text={'NAVER'}
          onPressed={() => loginWithSocial(SignInMethod.NAVER)}
        />
      </View>

      <View style={{ flexDirection: 'row', marginTop: 24 }}>
      <NoBorderButton text={'Guest signIn'} onPressed={() => loginWithGuest()}/>
        <NoBorderButton text={'Register now!'} onPressed={() => { navigation.navigate('Register'); }}/>
        <NoBorderButton text={'Forgot password'}  onPressed={() => { navigation.navigate('ForgotPassword'); }}/>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <NoBorderButton text={'Reset password'} onPressed={() => { navigation.navigate('ResetPassword'); }}/>
        <NoBorderButton text={'Find my account'} onPressed={() => { navigation.navigate('FindAccount'); }} />
      </View>
    </View>
  );
};

const PhoneLoginView = (props: any) => {
  const { navigation, style } = props;
  const [phone, setPhone] = useState('+84111111111');

  const loginWithPhone = async () => {
    await PhoneLoginSupport.getInstance().signPhoneNumber(phone);
    console.log("@phone");
    navigation.navigate('Code', { phone });
  }

  return <View style={{ ...style, flexDirection: 'row',  alignItems: 'center' }}>
    <TextInput
        style={styles.inputStyle}
        maxLength={12}
        placeholder="phone number"
        value={phone}
        onChangeText={(v) => setPhone(v)}
      />
      <Button text={"Go >>"} onPressed={loginWithPhone}/>
  </View>;
};

const UsernamePasswordLoginView = (props: any) => {
  const { signInWithPassword } = useAuthorization();
  const { navigation, style } = props;
  const [username, setUsername] = useState('hitek1@gmail.com');
  const [password, setPassword] = useState('11111111');

  const handleLoginButton = async () => {
    console.log("@login");
    try {
      const account = await signInWithPassword(username, password);
      console.log(account);
      navigation.navigate('Home');
    } catch(e) {

    }
  };

  return (
    <View style={style}>
      <TextInput
        style={styles.inputStyle}
        placeholder="username"
        value={username}
        onChangeText={(v) => setUsername(v)}
      />
      <TextInput
        style={styles.inputStyle}
        placeholder="password"
        value={password}
        onChangeText={(v) => setPassword(v)}
      />
      <Button text={'Login'} onPressed={handleLoginButton} />
    </View>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    fontSize: 18,
    minWidth: 300,
    marginVertical: 4,
    alignSelf: 'center',
    paddingHorizontal: 12,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 12,
  },
  socialContainer: {
    flexDirection: 'row',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  detailsContainer: {
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  button: {
    margin: 24,
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 20,
  },
  text: {
    textAlign: 'center',
  },
});

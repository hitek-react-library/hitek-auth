import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { useEffect } from 'react';
import { useAuthorization } from 'hitek-auth';

export const SplashScreen = (props: any) => {
  const { navigation } = props;
  const { autoSignIn } = useAuthorization();
  useEffect(() => {
    setTimeout(async () => {
      const success = await autoSignIn();
      if (success) {
        navigation.replace('Home');
      } else {
        navigation.replace('Login');
      }
      
    }, 1000);
  }, []);
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Welcome to Hitek Library</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  text: {
    textAlign: 'center',
  },
});

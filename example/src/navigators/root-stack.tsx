import React from 'react';
import { ROUTE_KEY } from './router-key';
import {
  createNativeStackNavigator,
  // NativeStackNavigationProp,
  NativeStackScreenProps,
} from 'react-native-screens/native-stack';
import { Text, TouchableOpacity } from 'react-native';
import { HomeScreen } from '../screens/home_sceen';
import { LoginScreen } from '../screens/login_screen';
import { SplashScreen } from '../screens/splash_screen';
import { VerifyCodePage } from '../screens/verify_code_page';
import { ResetPasswordScreen } from '../screens/reset_pwd_screen';
import { FindAccountScreen } from '../screens/find_account_screen';
import { ForgotPasswordScreen } from '../screens/forgot_pwd_screen';
import { RegisterScreen } from '../screens/register_screen';

export type RootStackScreensParams = {
  TestList: undefined | {};
  Splash: undefined | {};
  Home: undefined | {};
  Login: undefined | {};

  PhoneLogin: undefined | {};
  CodeLogin: undefined | {};
  LocalNav: undefined | {};
  Empty: undefined | {};
  ResetPassword: undefined | {};
  FindAccount: undefined | {};
  ForgotPassword: undefined | {};
  Register: undefined | {};
  Code: undefined | {phone: string};
};

export type RootStackScreens = keyof RootStackScreensParams;

export type RootStackScreenProps<T extends RootStackScreens> =
  NativeStackScreenProps<RootStackScreensParams, T>;

const { Navigator, Screen } =
  createNativeStackNavigator<RootStackScreensParams>();

const RootStack = () => (
  <Navigator
    initialRouteName={'Splash'}
    screenOptions={{
      headerShown: false,
      stackAnimation: 'fade',
    }}
  >
    <Screen name={ROUTE_KEY.Splash} component={SplashScreen} />
    <Screen name={ROUTE_KEY.Home} component={HomeScreen} />
    <Screen name={ROUTE_KEY.Login} component={LoginScreen} />
    <Screen name={ROUTE_KEY.Code} component={VerifyCodePage} />
    <Screen name={ROUTE_KEY.ResetPassword} component={ResetPasswordScreen} />
    <Screen name={ROUTE_KEY.FindAccount} component={FindAccountScreen} />
    <Screen name={ROUTE_KEY.ForgotPassword} component={ForgotPasswordScreen} />
    <Screen name={ROUTE_KEY.Register} component={RegisterScreen} />
  </Navigator>
);

export default RootStack;

export const EmptyPage = (props: any) => {
  const {navigation} = props;
  return <TouchableOpacity onPress={() => {
    navigation.navigation();
  }}>
    <Text>Hello</Text>
  </TouchableOpacity>;
  
};

function createEnum<T extends { [P in keyof T]: P }>(o: T) {
  return o;
}
export const ROUTE_KEY = createEnum({
  // typed as {  Home: 'Home' }
  Drawer: 'Drawer',
  Splash: 'Splash',
  TestList: 'TestList',
  
  
  HomeHitek: 'HomeHitek',
  SettingHitek: 'SettingHitek',
  LoginHitek: 'LoginHitek',
  VerifyCodeHitek: 'VerifyCodeHitek',
  ChangePasswordHitek: 'ChangePasswordHitek',

  
  Login: 'Login',
  Home: 'Home',
  Code: 'Code',
  ResetPassword: 'ResetPassword',
  FindAccount: 'FindAccount',
  ForgotPassword: 'ForgotPassword',
  Register: 'Register'


});

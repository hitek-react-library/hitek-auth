import React, { useCallback } from 'react';
import { AuthProvider, setConfig } from 'hitek-auth';
import {
  initialWindowMetrics,
  SafeAreaProvider,
} from 'react-native-safe-area-context';
import { Provider as PaperProvider } from 'react-native-paper';
import {
  NavigationContainer,
  // NavigationContainerRef
} from '@react-navigation/native';
import RootStack from './navigators/root-stack';
import FlashMessage from 'react-native-flash-message';
import Spinner from './widgets/spinner';

export default function App() {
  React.useEffect(() => {
    setConfig({
      host: 'https://planpicker.hitek.com.vn:5001/api/v1',
      // host: 'https://langbiang.hitek.com.vn:7001/api/v1',
      log: true,
      google: {
        webClientId:
          '413650174244-25sjfhaopkp5dhm3ho5dhdevi4564715.apps.googleusercontent.com',
      },
      naver: {
        androidConfig: {
          kConsumerKey: 'xkJHYT4aCzIBk87MalHr',
          kConsumerSecret: 'B1VhWfAEbM',
          kServiceAppName: 'PlanPicker',
        },
        iosConfig: {
          kConsumerKey: 'xkJHYT4aCzIBk87MalHr',
          kConsumerSecret: 'B1VhWfAEbM',
          kServiceAppName: 'PlanPicker',
          kServiceAppUrlScheme: 'testapp', // only for iOS
        },
      },
    });
  }, []);

  const onReady = useCallback(async () => {
  }, []);

  return (
    <AuthProvider>
      <SafeAreaProvider initialMetrics={initialWindowMetrics}>
        <PaperProvider>
          <NavigationContainer onReady={onReady}>
            <Spinner />
            <RootStack />
            <FlashMessage />
          </NavigationContainer>
        </PaperProvider>
      </SafeAreaProvider>
    </AuthProvider>
  );
}
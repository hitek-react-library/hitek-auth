const images = {
    layer: require('./layer.png'),
    testimgques: require('./testQuestion.png'),
    routine_test_1: require('./routine_test1.png'),
    routine_test_2: require('./routine_test2.png'),
    testLetter: require('./weelo-newsletter.png'),
    testAvar: require('./profile_ava.jpeg'),
    logo_alpha: require('./LogoWeelo.png'),
    image_mock: require('./image_mock_1.png'),
    image_mock_2: require('./image_mock_2.png'),
    loading: require('./loading.png'),
    video_thumb: require('./no_image.png'),
    todayRoutine: require('./todayRoutine.jpg'),
    splash: require('./splash.mp4'),
};

export default images;

import React from 'react';
import Modal from 'react-native-modal';
const { UIActivityIndicator } = require('react-native-indicators');
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Props {}
interface State {
    visible: boolean;
}
export default class Spinner extends React.PureComponent<Props, State> {
    static instance: any;

    constructor(props: Props) {
        super(props);
        Spinner.instance = this;
        this.state = {
            visible: false,
        };
    }

    static show() {
        if (Spinner.instance) {
            !Spinner.instance.state.visible && Spinner.instance.setState({ visible: true });
        }
    }

    static hide() {
        if (Spinner.instance) {
            Spinner.instance.setState({ visible: false });
        }
    }

    render() {
        if (Spinner?.instance?.state?.visible) {
            return (
                <Modal
                    animationInTiming={0}
                    animationOutTiming={0}
                    animationIn={'fadeIn'}
                    animationOut={'fadeOut'}
                    isVisible={this.state.visible || false}
                    style={{
                        margin: 0,
                        padding: 0,
                        flex: 1,
                        alignItems: 'center',
                        backgroundColor: '#000000aa',
                        justifyContent: 'center',
                    }}
                >
                    <UIActivityIndicator color="white" />
                </Modal>
            );
        }
        return null;
    }
}
